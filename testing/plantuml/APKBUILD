# Maintainer: Krystian Chachuła <krystian@krystianch.com>
pkgname=plantuml
pkgver=1.2022.0
pkgrel=0
pkgdesc="Draw UML diagrams, using a simple and human readable text description"
url="https://plantuml.com/"
# no mips64 and riscv64 because of apache-ant
# there's no more 32-bit bit
arch="noarch !x86 !armhf !armv7 !mips64 !riscv64"
license="GPL-3.0-or-later"
depends="busybox java-common java-jre"
makedepends="apache-ant"
options="!check"  # no tests provided
source="https://downloads.sourceforge.net/plantuml/plantuml-$pkgver.tar.gz
	plantuml.run"

build() {
	ant dist
}

package() {
	install -Dm644 plantuml.jar \
		"$pkgdir/usr/share/java/plantuml.jar"
	install -Dm755 "$srcdir/plantuml.run" \
		"$pkgdir/usr/bin/plantuml"
}

sha512sums="
8fdead5696e6ddceb947a2d67aad4dce5d82511b795c5f5a5196fe8d7abc7c7de72f56d2982c82a3b784139dccaec53de7510f757eda758ab5f300b440a278b1  plantuml-1.2022.0.tar.gz
cab64f1676cbd495752eda31f2ee4ab7d976253fe523a6db5b55fd9a165f1530da0d34196494ce38b0aaad6525f4f1e1e7fe60307ad220146b5a654d96c9d060  plantuml.run
"
