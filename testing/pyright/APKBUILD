# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=pyright
pkgver=1.1.209
pkgrel=0
pkgdesc="Static type checker for Python"
url="https://github.com/microsoft/pyright"
license="MIT"
# riscv64: blocked by nodejs
# s390x: tests fail
arch="noarch !riscv64 !s390x"
depends="nodejs"
makedepends="npm"
subpackages="$pkgname-doc"
source="https://github.com/microsoft/pyright/archive/$pkgver/pyright-$pkgver.tar.gz"

prepare() {
	default_prepare
	npm ci
}

build() {
	cd packages/pyright
	npm run build
}

check() {
	cd packages/pyright-internal
	npm run test
}

package() {
	local destdir=/usr/lib/node_modules/pyright

	install -d \
		"$pkgdir"/usr/bin \
		"$pkgdir"/$destdir \
		"$pkgdir"/usr/share/doc/pyright

	cp -r docs/* LICENSE.txt "$pkgdir"/usr/share/doc/pyright

	cd packages/pyright
	cp -r dist index.js langserver.index.js package.json "$pkgdir"/$destdir

	ln -s $destdir/index.js "$pkgdir"/usr/bin/pyright
	ln -s $destdir/langserver.index.js "$pkgdir"/usr/bin/pyright-langserver
}

sha512sums="
4a78dd30195d891bb4b899aaec3c3815cbeb10880455682b950fba32472e4bc780a4da478cdd973231527642f03b54ee8e6da094c1706374151805aab1735743  pyright-1.1.209.tar.gz
"
