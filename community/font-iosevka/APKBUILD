# Contributor: psykose <alice@ayaya.dev>
# Maintainer: psykose <alice@ayaya.dev>

pkgname=font-iosevka
pkgver=11.2.6
pkgrel=0
pkgdesc="Versatile typeface for code, from code."
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab:curly_slab
	$pkgname-aile
	$pkgname-etoile
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-aile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-etoile-$pkgver.zip
	"

builddir="$srcdir"

package() {
	depends="
		$pkgname-base
		$pkgname-slab
		$pkgname-curly
		$pkgname-curly-slab
		$pkgname-aile
		$pkgname-etoile
	"

	mkdir -p "$pkgdir"/usr/share/fonts/TTC
	mv "$builddir"/*.ttc "$pkgdir"/usr/share/fonts/TTC
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	amove usr/share/fonts/TTC/iosevka.ttc
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	amove usr/share/fonts/TTC/iosevka-slab.ttc
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	amove usr/share/fonts/TTC/iosevka-curly.ttc
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	amove usr/share/fonts/TTC/iosevka-curly-slab.ttc
}

aile() {
	pkgdesc="$pkgdesc (Iosevka Aile)"
	amove usr/share/fonts/TTC/iosevka-aile.ttc
}

etoile() {
	pkgdesc="$pkgdesc (Iosevka Etoile)"
	amove usr/share/fonts/TTC/iosevka-etoile.ttc
}

sha512sums="
5d9d789f83da9fc98a1df6d127fbc758d3f90b61b5a66bb8f94e2f8b215589ff2410c0bf50d5ff6e9a8081691205aa5b93d31c7273374bf07d3c9aae2e1956ec  super-ttc-iosevka-11.2.6.zip
b2f257b5825fddeb6db5f70ae0594fbc4a5977b3446ead1944d40fe6d1dc3f6604218108a94201a098d6e780924f124c7fead4a4a5461dd77b6493faf7f0ae9a  super-ttc-iosevka-slab-11.2.6.zip
02a4a1ac699daa732701f64375c8f8d5c09432845f70bc355f9ae7e62c2f976d7fbb882099af44f7697c033d6b414972dcc2fbc68f059326dffaad0598c0a186  super-ttc-iosevka-curly-11.2.6.zip
a86645dea0d35aa02267eafe7aec2767e9d73a44ca3519c766535d0847dc7ec689899e7b08dfbcb18f12f382a0b65f41c3de67e6a868397107252dc77ea19d3f  super-ttc-iosevka-curly-slab-11.2.6.zip
053762d558032bddb539c71c0cb2e013dfcecc622cdb53da58511557dc34c7c95dde9ac34f0f79cdb8987f09f657297685a4ee2e41769156fb0a5d59010ad605  super-ttc-iosevka-aile-11.2.6.zip
4bace9c74074494ed86414040bbe82f740c8aa96c4cbd3b495950cf3a538ce1cae9327e57847a70fa72ddca5cde16dafca31640de5d556ce45f55eb4e10c0b58  super-ttc-iosevka-etoile-11.2.6.zip
"
