# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=tree-sitter-javascript
pkgver=0.20.0
pkgrel=0
pkgdesc="JavaScript grammar for tree-sitter"
url="https://github.com/tree-sitter/tree-sitter-javascript"
arch="all"
license="MIT"
makedepends="tree-sitter-dev"
install_if="tree-sitter-grammars"
source="https://github.com/tree-sitter/tree-sitter-javascript/archive/rust-$pkgver/tree-sitter-javascript-$pkgver.tar.gz"
builddir="$srcdir/$pkgname-rust-$pkgver"
options="!check"  # no tests for shared lib

build() {
	cd src

	cc $CFLAGS -fPIC -Wall -std=c99 -c ./*.c
	cc $LDFLAGS -shared -Wl,--no-undefined -o lib$pkgname.so ./*.o
}

package() {
	install -D -m755 src/lib$pkgname.so -t "$pkgdir"/usr/lib/

	# Create symlink for Neovim
	install -d "$pkgdir"/usr/share/nvim/runtime/parser
	ln -s ../../../../lib/lib$pkgname.so \
		"$pkgdir"/usr/share/nvim/runtime/parser/${pkgname#tree-sitter-}.so
}

sha512sums="
778e0f2f5bf276d7ffb6eab0fe3f7473b56605de707c8e2f491fbb5826da94067b9f1bc476306013267001b5ecaab11a956e896b78520cdedd27006fac97fe41  tree-sitter-javascript-0.20.0.tar.gz
"
