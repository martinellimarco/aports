# Contributor: Nathan Johnson <nathan@nathanjohnson.info>
# Maintainer: Dave Hall <skwashd@gmail.com>
pkgname=composer
pkgver=2.2.4
pkgrel=0
pkgdesc="Dependency manager for PHP"
url="https://getcomposer.org/"
arch="noarch"
license="MIT"
_php=php8
depends="$_php $_php-phar $_php-curl $_php-iconv $_php-mbstring $_php-openssl $_php-zip"
checkdepends="git"
options="net"
source="$pkgname-$pkgver.phar::https://getcomposer.org/download/$pkgver/composer.phar"

# secfixes:
#   2.1.9-r0:
#     - CVE-2021-41116
#   2.0.13-r0:
#     - CVE-2021-29472

check() {
	cd "$srcdir"
	$_php $pkgname-$pkgver.phar -Vn
	$_php $pkgname-$pkgver.phar -n diagnose || true # fails as pub-keys are missing
}

package() {
	install -m 0755 -D "$srcdir"/$pkgname-$pkgver.phar "$pkgdir"/usr/bin/$pkgname.phar
	printf "#!/bin/sh\n\n/usr/bin/php8 /usr/bin/composer.phar \"\$@\"\n"\
		> "$pkgdir"/usr/bin/$pkgname
	chmod +x "$pkgdir"/usr/bin/$pkgname
}

sha512sums="
3111d740b5f08075ac408284bdfdabb8334cce1266f8b27aaafdcfbde81cfe4eb25dba6df0464d4b2e50f8c44d71fc3ae3dabba0117d6a0f6a569f9998012b82  composer-2.2.4.phar
"
